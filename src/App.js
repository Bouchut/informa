import React,{Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Weather from './components/Weather';
import News from './components/News';
import Homepage from './components/Homepage';
import Images from './components/Images';
import Messages from './components/Messages';

class App extends Component {
  render(){
    return (
      
      <div>
        <Router>
              <Switch>
                <Route path="/" exact component={Homepage}/>
                <Route path="/Weather" exact component={Weather}/>
                <Route path="/News" exact component={News}/> 
                <Route path="/Images" exact component={Images}/>
                <Route path="/Messages" exact component={Messages}/>
              </Switch>
        </Router>

      </div>
    );
  }
}

export default App;
