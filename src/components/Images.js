import React, { Component } from 'react'
import HomeButton from './HomeButton'

export default class Images extends Component {
    render() {
        return (
            <div>
                <header className="App-header">
                    <h1>INFORMA</h1>
                    <h2>Welcome on Weather page</h2>
                    <HomeButton />
                </header>

                <div className="App-body"></div>
            </div>
        )
    }
}
