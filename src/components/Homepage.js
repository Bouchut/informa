import React, { Component } from 'react'
import Card from './Card';
// import { LinkContainer } from 'react-router-bootstrap'

import './Homepage.css'

import data from '../data/cardData.json'

export default class Homepage extends Component {
    state = {
        cardsArray : data.cards
      }
    
    render() {
        return (
            <div className="App">

                <header className="App-header">
                <h1>INFORMA</h1>
                <h2>Welcome Florian</h2>
                </header>

                <div className="App-body">
                    <div className="Cards-list">
                        {this.state.cardsArray.map(card => (
                            <Card key={card.id} title={card.title} src={card.src} alt={card.alt}/>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}
