import React from "react";
import "./Card.css"
import {NavLink} from 'react-router-dom'

class Card extends React.Component {
  
    render() {
        return (
          <div className="card">
            <NavLink to={`/${this.props.title}`}>
              <div className="card-header">
                <h5 className="card-title">{this.props.title}</h5>
              </div>
              <div className="card-body">
                <img className="card-img-bottom" src={this.props.src} alt={this.props.alt}/>
              </div>
            </NavLink>
          </div>
      );
    }
}

export default Card
