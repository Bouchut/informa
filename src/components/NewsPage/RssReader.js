import React, { useState, useEffect } from 'react'
import "./RssReader.css"

const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";
// https://www.irishtimes.com/rss/activecampaign-business-today-digest-more-from-business-1.3180340
// https://www.independent.ie/breaking-news/rss/

export default function RssReader() {
    const [feed, setFeed] = useState([]);
    const [description, setDescription] = useState("");


    useEffect(() => {
        let Parser = require('rss-parser');
        let parser = new Parser();

        //ne pas oublier de rajouer un if catch
        (async () => {

            let rssfeed = await parser.parseURL(CORS_PROXY + 'https://www.independent.ie/breaking-news/rss/');
            //console.log(rssfeed);

            // rssfeed.items.forEach(item => {
            //     console.log(item.title + ':' + item.link)
            //   });

            setFeed(rssfeed.items);
            setDescription(rssfeed.title);
        })();

    }, [])



    return (
        <div>
            <h3><b>{description}</b></h3>
            <div className="card-list">
                {feed.map((item, index) => (
                    <div className="card2" key={index}>
                        <div className="card2-body">
                            <h5 className="card2-title">{item.title}</h5>
                            <p className="card-text">{item.content}</p>
                            <a href={item.link} className="card-link">Open article</a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}
