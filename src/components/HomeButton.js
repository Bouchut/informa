import React, { Component } from 'react'
import './HomeButton.css'
import {NavLink} from 'react-router-dom'

export default class HomeButton extends Component {
    render() {
        return (
            <div>
                <NavLink to="/">
                    <button type="button" className="btn btn-primary btn-lg">Back</button>
                </NavLink>
            </div>
        )
    }
}
