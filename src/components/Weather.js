import React, { Component } from 'react'
import HomeButton from './HomeButton'
import axios from 'axios'
import WeatherWidget from './WeatherWidget/WeatherWidget'

export default class Weather extends Component {
    state = {
        weather: []
    }

    componentDidMount() {
        axios.get(`https://api.openweathermap.org/data/2.5/weather/?q=Dublin&units=metric&APPID=42ac8838a5eb1a998b9ac69f79eb45c7`)
            .then(res => {
                const weather = res.data;
                // console.log(weather);
                this.setState({ weather });
            })
    }
    // https://api.openweathermap.org/data/2.5/onecall?lat=60.99&lon=30.9&appid=42ac8838a5eb1a998b9ac69f79eb45c7

    
    render() {
        return (
            <div>
                <header className="App-header">
                    <h1>INFORMA</h1>
                    <h2>Welcome on Weather page</h2>
                    <HomeButton />
                </header>

                <div className="App-body">
                    {/* <p>{this.state.weather.timezone}</p> */}
                    {/* <p>{JSON.stringify(this.state.weather.hourly.dt, null, 2)}</p> */}
                    {/* <p>{new Date(this.state.weather.dt*1000).toDateString()}</p>

                    <div class="widget">
                        <div class="left-panel panel">
                            <div class="date">
                                Monday, 20 Aug 2018
                            </div>
                            <div class="city">
                                Mumbai
                            </div>
                            <div class="temp">
                                <img src="https://s5.postimg.cc/yzcm7htyb/image.png" alt="" width="60" />
                                    27&deg;
                            </div>
                        </div>
                        <div class="right-panel panel">
                            <img src="https://s5.postimg.cc/lifnombwz/mumbai1.png" alt="" width="160" />
                        </div>
                    </div>
                     */}
                    <WeatherWidget/>

                </div>
            </div>
        )
    }
}


