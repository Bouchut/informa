import React, { Component } from 'react'
import HomeButton from './HomeButton'
import RssReader from './NewsPage/RssReader'


export default class News extends Component {
    render() {
        return (
            <div>
                <header className="App-header">
                    <h1>INFORMA</h1>
                    <h2>Welcome on News page</h2>
                    <HomeButton />
                </header>
                <div className="App-body">
                    <RssReader />
                </div>
            </div>
        )
    }
}
